function sieve (array) {
	var result = array.filter(
		function(obj, index) {
			return array.indexOf(obj) === index;
		}
	);
	return result;
}

function distance(first, second){
	if (!(Array.isArray(first) && Array.isArray(second))) {
		throw new Error("InvalidType");
	}

	if (!(first.length && second.length)) {
		return 0;
	}

	var a = sieve(first);
	var b = sieve(second);
	var ctr = 0;

	for (var i = 0; i < a.length; i++) {
		for (var j = 0; j < b.length; j++) {
			if (a[i] !== b[j]) {
				ctr++;
				if (typeof a[i] != typeof b[j]) {
					ctr++;
				}
			}
		}
	}

	return ctr;
}

module.exports.distance = distance